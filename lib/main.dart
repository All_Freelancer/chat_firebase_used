import 'package:chat_signalr_app/src/ui/pages/chat_screen.dart';
import 'package:flutter/material.dart';

//******************************* CLASS MAIN ***********************************
void main() => runApp(MyApp());

//********************************* THEME DATA *********************************
//::::::::::::::: IOS 
final ThemeData kIOSTheme = ThemeData(
  primarySwatch: Colors.orange,
  primaryColor: Colors.grey[100],
  primaryColorBrightness: Brightness.light,
);

//:::::::::::::::::: ANDROID
final ThemeData kDefaultTheme = ThemeData(
  primarySwatch: Colors.purple,
  accentColor: Colors.orangeAccent[400],
);

//****************************** CLASS MY APP **********************************
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Chat App",
      debugShowCheckedModeBanner: false,
      theme: Theme.of(context).platform == TargetPlatform.iOS
          ? kIOSTheme
          : kDefaultTheme,

      home: ChatScreen(),
    );
  }
}
//******************************************************************************
