import 'package:chat_signalr_app/src/ui/widgets/chat_message.dart';
import 'package:chat_signalr_app/src/ui/widgets/text_composer.dart';
import 'package:chat_signalr_app/cloud_firestore.dart';
import 'package:flutter/material.dart';

//****************************** CLASS CHAT SCREEN *****************************
class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

//********************************** STATE CHAT SCREEN *************************
class _ChatScreenState extends State<ChatScreen> {


  //******************************* ROOT WIDGETS *******************************
  @override
  Widget build(BuildContext context) {

    return SafeArea(
      bottom: false,
      top: false,

      child: Scaffold(

        //:::::::::::::::::::::::::::::: APP BAR :::::::::::::::::::::::::
        appBar: AppBar(
          title: Text("Chat App"),
          centerTitle: true,
          elevation:
          Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
        ),

        //:::::::::::::::::::::::::::::::: BODY :::::::::::::::::::::::::::
        body: Column(
          children: <Widget>[
            //------------------ CHAT - MESSAGE LIST --------------
            Expanded(
              child: StreamBuilder(
                  stream: Firestore.instance.collection("messages").snapshots(),  // FIRE BASE PACKAGE
                  builder: (context, snapshot) {
                    switch(snapshot.connectionState){
                      case ConnectionState.none:
                      case ConnectionState.waiting:
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      default:
                        return ListView.builder(
                            reverse: true,
                            itemCount: snapshot.data.documents.length,
                            itemBuilder: (context, index) {
                              List r = snapshot.data.documents.reversed.toList();
                              return ChatMessage(r[index].data);
                            }
                        );
                    }
                  }
              ),
            ),

            //--------------------------- SEPARATE -----------------------
            Divider(
              height: 1.0,
            ),

            //------------------------- SEND MESSAGE ---------------------
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).cardColor,
              ),
              child: TextComposer(),
            )
          ],
        ),
      ),
    );
  }
}
//******************************************************************************
